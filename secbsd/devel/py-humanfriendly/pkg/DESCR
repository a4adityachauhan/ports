The functions and classes in the humanfriendly package can be used
to make text interfaces more user friendly. Some example features:

Parsing and formatting numbers, file sizes, pathnames and timespans
in simple, human friendly formats.

Easy to use timers for long running operations, with human friendly
formatting of the resulting timespans.

Prompting the user to select a choice from a list of options by typing
the options number or a unique substring of the option.

Terminal interaction including text styling ANSI escape sequences,
user friendly rendering of usage messages and querying the terminal
for its size.

The humanfriendly package is currently tested on Python 2.6, 2.7, 3.4,
3.5, 3.6, 3.7 and PyPy (2.7) on Linux and Mac OS X. 

While the intention is to support Windows as well, you may encounter
some rough edges.
